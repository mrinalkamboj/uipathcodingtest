﻿using System.Linq;

namespace UiPathCodingTest
{
    /// <summary>
    /// Coding Test Ui Path
    /// </summary>
    public class IntegerWalls
    {
        /// <summary>
        /// For a Input Int[] with Positive integers, Calcualte the total water stored between walls.
        /// Process the result using 2 Int[] in O(2N) time complexity
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public int AggregatedQuantity(int[] input)
        {
            // Final array to aggrgate all the Data
            var final = new int[input.Length];

            // Set the Boundary values
            final[0] = 0;
            final[input.Length - 1] = 0;

            // Forward movement current highest
            var currentHighest = input[0];

            // Traverse between "0 to Length-1" (Both Exclusive)
            for (var x = 1; x < input.Length - 1; x++)
            {
                // Set the Final array Index Value
                final[x] = currentHighest - input[x];

                // Replace Highest value if the index value is greater
                if (input[x] > currentHighest)
                    currentHighest = input[x];
            }

            // Reverse movement current highest
            currentHighest = input[input.Length - 1];

            // Traverse between "Length-1 to 0" (Both Exclusive)
            for (var x = input.Length - 2; x > 0; x--)
            {
                // Fetch the Calculated Value as the Differnce with Highest Index
                var calculatedValue = currentHighest - input[x];

                // Replace Calculated Value at Final Index if Smaller
                if (calculatedValue < final[x])
                    final[x] = calculatedValue;

                // Replace Highest value if the index value is greater
                if (input[x] > currentHighest)
                    currentHighest = input[x];
            }

            // Return the Result with negative values replaced by 0 for Sum (To Exclude)
            return final.Sum(x => x < 0 ? 0 : x);
        }

        /// <summary>
        /// For a Input Int[] with Positive integers, Calcualte the total water stored between walls.
        /// Process the result using 2 Int[] in O(N) time complexity 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public int AggregatedQuantityImproved(int[] input)
        {
            // Final array to aggrgate all the Data
            var final = new int[input.Length];

            // Set the Boundary values
            final[0] = 0;
            final[input.Length - 1] = 0;

            // Forward movement current highest
            int currentHighestForward = input[0];
            int currentHighestReverse = input[input.Length - 1];

            // Traverse between "0 to Length-1" (Both Exclusive)
            for (var x = 1; x < input.Length - 1; x++)
            {
                // Set the Final array Index Value
                var calculatedValueForward = currentHighestForward - input[x];

                if (x <= (input.Length - 1 - x))
                    final[x] = calculatedValueForward;
                else
                {
                    if (calculatedValueForward < final[x])
                        final[x] = calculatedValueForward;
                }

                // Replace Highest value if the index value is greater
                if (input[x] > currentHighestForward)
                    currentHighestForward = input[x];

                // Set the Final array Index Value
                var calculatedValueReverse = currentHighestReverse - input[input.Length - 1 - x];

                if ((input.Length - 1 - x) > x)
                    final[input.Length - 1 - x] = calculatedValueReverse;
                else
                {
                    if (calculatedValueReverse < final[input.Length - 1 - x])
                        final[input.Length - 1 - x] = calculatedValueReverse;
                }
                // Replace Highest value if the index value is greater
                if (input[input.Length - 1 - x] > currentHighestReverse)
                    currentHighestReverse = input[input.Length - 1 - x];
            }

            // Return the Result with negativr values replaced by 0 for Sum
            return final.Sum(x => (x < 0) ? 0 : x);
        }
    }
}


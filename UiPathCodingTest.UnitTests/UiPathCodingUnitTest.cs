﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UiPathCodingTest
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void Test1()
        {
            // Arrange
            var input = new[] { 10, 8, 5, 4, 12, 6, 14 };
            // Act
            var test = new IntegerWalls();
            var aggregtedQuantity = test.AggregatedQuantity(input);

            //Assert
            Assert.AreEqual(19,aggregtedQuantity);
        }

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test1Improved()
        {
            // Arrange
            var input = new[] { 10, 8, 5, 4, 12, 6, 14 };
            // Act
            var test = new IntegerWalls();
            var aggregtedQuantity = test.AggregatedQuantityImproved(input);

            //Assert
            Assert.AreEqual(19, aggregtedQuantity);
        }

        [TestMethod]
        public void Test2()
        {
            // Arrange
            var input = new[] { 10, 8, 6, 4, 2, 1 };
            // Act
            var test = new IntegerWalls();
            var aggregtedQuantity = test.AggregatedQuantity(input);

            //Assert
            Assert.AreEqual(0, aggregtedQuantity);
        }

        [TestMethod]
        public void Test2Improved()
        {
            // Arrange
            var input = new[] { 10, 8, 6, 4, 2, 1 };
            // Act
            var test = new IntegerWalls();
            var aggregtedQuantity = test.AggregatedQuantityImproved(input);

            //Assert
            Assert.AreEqual(0, aggregtedQuantity);
        }


        [TestMethod]
        public void Test3()
        {
            // Arrange
            var input = new[] { 10, 8, 6, 4, 2, 10 };
            // Act
            var test = new IntegerWalls();
            var aggregtedQuantity = test.AggregatedQuantity(input);

            //Assert
            Assert.AreEqual(20, aggregtedQuantity);
        }

        [TestMethod]
        public void Test3Improved()
        {
            // Arrange
            var input = new[] { 10, 8, 6, 4, 2, 10 };
            // Act
            var test = new IntegerWalls();
            var aggregtedQuantity = test.AggregatedQuantityImproved(input);

            //Assert
            Assert.AreEqual(20, aggregtedQuantity);
        }
    }
}
